<?PHP

include("common.php");
require_once('vendor/autoload.php');
use Aws\S3\S3Client;

// Get the query string parameter, if it's not there, use the hardcoded default.
$s3Threshold = $_GET['size'];
if (empty($s3Threshold) || !is_numeric($s3Threshold)) {
	$s3Threshold = 3000000000;
	//$s3Threshold = 15000;
}

// Get the uploaded filenames and sizes from the database and total up the sizes.
$query = "SELECT filename, sizeInBytes FROM audit_upload WHERE deleted = 0 ORDER BY date";
$uploadFiles = issue_db_query($query);
$totalFileSize = 0;
while($row = mysql_fetch_assoc($uploadFiles)) {
	$totalFileSize += $row['sizeInBytes'];
}

// Display the threshold and the total file size.
print("Threshold in bytes: " . $s3Threshold . "<br>");
print("Total size of files in bytes: " . $totalFileSize . "<br><br>");

// Reset the files recordset to the beginning.
mysql_data_seek($uploadFiles, 0);

// Initialize the S3 variable.
$s3 = null;

// Iterate through all the uploaded files until the total size of all files is under the threshold.
while (($row = mysql_fetch_assoc($uploadFiles)) && ($totalFileSize > $s3Threshold)) {
	print("Deleting a file to get below threshold...<br>");
	
	// Get the oldest file and its size from the database.
	$filename = $row['filename'];
	$filesize = $row['sizeInBytes'];
	
	print(sprintf("File: %s  Size: %d<br><br>", $filename, $filesize));
	
	// Get a S3Client if we haven't already.
	if ($s3 === null) {
		$s3 = S3Client::factory(array('key'    => AWS_PUBLIC_KEY,
									'secret' => AWS_SECRET_KEY,
									'region' => BUCKET_REGION));
	}

	// Delete the object in S3.
	$result = $s3->deleteObject(array('Bucket' => BUCKET_NAME, 'Key'    => $filename));

	// NOTE: One enhancement would be to only mark the record as deleted so that tracability of
	// files that are no longer in S3 bucket is still maintained.
	// Delete the row in the upload table.
	//$query = sprintf("DELETE FROM audit_upload WHERE filename LIKE '%s'", $filename);
	$query = sprintf("UPDATE audit_upload SET deleted = 1 WHERE filename LIKE '%s'", $filename);
	$result = issue_db_query($query);

	// Deduct the deleted file size from the total. 
	$totalFileSize -= $filesize;
	
	// Show new total.
	print("Threshold in bytes: " . $s3Threshold . "<br>");
	print("Total size of files in bytes: " . $totalFileSize . "<br><br>");
}

// NOTE: Doing this will reset the 'total system views' count.  If that's important, don't delete
// those records.
// Delete orphaned records in the views table.
//print("Deleting records from the view table that no longer reference the upload table...<br>");
// Do a general cleanup of the views table.
//$query = "DELETE FROM audit_views WHERE filename NOT IN (SELECT filename FROM audit_upload)";
//$result = issue_db_query($query);

?>