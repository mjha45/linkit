<?PHP

include("common.php");

// Get the uploaded filenames and sizes from the database and total up the sizes.
$query = "SELECT @total := FORMAT(SUM(sizeInBytes) / 1000000, 2) as 'Size in MB', @numfiles := COUNT(sizeInBytes) as 'Number of Files', @total / @numfiles as 'Avg file size in MB' from audit_upload WHERE deleted = 0";
$uploadFiles = issue_db_query($query);
$row = mysql_fetch_assoc($uploadFiles);

//print($query . "<br><br>");
//print(var_dump($row) . "<br><br>");

print("Total size of all files in upload table (in MB): " . $row['Size in MB'] . "<br>");
print("Number of files in the upload table: " . $row['Number of Files'] . "<br>");

$avgSize = $row['Size in MB'] / $row['Number of Files'];
print("Avg filesize in MB: " . number_format($avgSize, 2) . "<br><br>");

// This section will display the size of the RDS database (very small).
$query = "SELECT table_schema 'Data Base Name', FORMAT(sum( data_length + index_length ) / 1024 / 1024, 2) 'Data Base Size in MB' 
FROM information_schema.TABLES WHERE table_schema LIKE 'cclinkit'";
$result = issue_db_query($query);
$row = mysql_fetch_assoc($result);
print("Total size of audit log DB in MB: " . $row['Data Base Size in MB'] . "<br><br>");

// This section is for the total all-time bandwidth usage for views and
// the total all-time number of views.
$query = "SELECT SUM(audit_upload.sizeInBytes) / 1000000 as 'Size In MB', COUNT(audit_views.filename) as 'Number of Views' FROM audit_upload, audit_views WHERE audit_upload.filename LIKE audit_views.filename";
$result = issue_db_query($query);
$row = mysql_fetch_assoc($result);
print("Total (all time) transfer bandwidth out for views of images in MB: " . $row['Size In MB'] . "<br>");
print("Total (all time) number of views: " . $row['Number of Views'] . "<br><br>");

// This section is for this month's view bandwidth usage and
// this month's view total.
$query = "SELECT SUM(audit_upload.sizeInBytes) / 1000000 AS 'Size In MB', COUNT(audit_views.filename) AS 'Number of Views' FROM audit_upload, audit_views WHERE audit_upload.filename LIKE audit_views.filename AND MONTH(audit_views.date) = MONTH(CURDATE()) AND YEAR(audit_views.date) = YEAR(CURDATE())";
$result = issue_db_query($query);
$row = mysql_fetch_assoc($result);
// Note: S3 free tier allows for 15GB a month.
print("This month's transfer bandwidth out for views of images in MB (AWS allows 15GB): " . $row['Size In MB'] . "<br>");
print("This month's number of views: " . $row['Number of Views'] . "<br><br>");

?>