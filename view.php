<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Linkit! Image Viewer</title>
	<style>
		body {
			color: #fff; background-color: #333; font-family: Arial, sans-serif; font-size: large; text-align: center;
		}
		a   {
			color: #fff; background-color: transparent;
		}
		img {
			image-orientation: from-image;
		}
	</style>
</head>
<body>
<?PHP

include("common.php");
// Load in the AWS library.
require_once('vendor/autoload.php');
use Aws\S3\S3Client; 


// Get the IP address of the client.
$ipOfClient = get_client_ip_address();

// Debug IP addresses
//print("<br><br>" . $ipOfClient . "<br><br>");
//print("<br><br>'HTTP_CLIENT_IP': " . $_SERVER['HTTP_CLIENT_IP'] . "<br>");
//print("'HTTP_X_FORWARDED_FOR': " . $_SERVER['HTTP_X_FORWARDED_FOR'] . "<br>");
//print("'REMOTE_ADDR': " . $_SERVER['REMOTE_ADDR'] . "<br><br>");


try
{
	// Get a S3Client instance for our configuration.
	$s3 = S3Client::factory(array('key'    => AWS_PUBLIC_KEY,
								'secret' => AWS_SECRET_KEY,
								'region' => BUCKET_REGION));
}
catch (Exception $e)
{
	displayErrorAndExit("Error creating S3 client:", $e->getMessage());
}

// Get the query string parameter
$imageFileName = $_GET['i'];

$errorSummary = "No or bad image identifier provided.";
$errorDetail = "To view Linkit! images, a valid, uploaded image identifier must be provided.";
// Do some simple and quick checks on the image filename that was passed in.
if (empty($imageFileName) || !is_string($imageFileName)) {
	displayErrorAndExit($errorSummary, $errorDetail);
}
// Now that we know it's populated and a string, make it lowercase.
$imageFileName = strtolower($imageFileName);
// Our image file names are always 17 characters, including extension.
if (strlen($imageFileName) != 17) {
	displayErrorAndExit($errorSummary, $errorDetail);
}

// Get the file extension.
$ext = substr(strrchr($imageFileName,'.'),1);
// Make sure it's one of JPG, GIF, or PNG.
if ($ext != 'jpg' && $ext != 'gif' && $ext != 'png') {
	displayErrorAndExit($errorSummary, $errorDetail);
}

// TODO: Consider keeping the database connection open for the whole procedure.
// Instead of checking bucket, let's check the database (cheaper and probably faster).
$selectStmt = sprintf("SELECT COUNT(*) FROM audit_upload WHERE filename LIKE '%s' AND deleted = 0", $imageFileName);
$result = issue_db_query($selectStmt);
$imageFound = mysql_result($result, 0, 0);
if (!$imageFound)
{
	displayErrorAndExit("Image could not be found in storage.", "Either the link provided is bad or the image duration has expired and it's been deleted.");
}

// Build the auditing insertion statement.
$insertStmt = sprintf("INSERT INTO audit_views (ip, filename) VALUES ('%s', '%s')", $ipOfClient, $imageFileName);

// Write the audit information to the database.
issue_db_query($insertStmt);

print("<h1>Linkit! Image Viewer</h1><br>");

//Display number of times image has been viewed
$viewQuery = sprintf("SELECT COUNT(*) FROM audit_views WHERE filename LIKE '%s'", $imageFileName);
$viewResult = issue_db_query($viewQuery);
$viewCount = mysql_result($viewResult, 0, 0);
if ($viewCount == 1) {
print("This image has been viewed 1 time<br><br>");
}
else{
print("This image has been viewed " . $viewCount . " times<br><br>");
}


// Display the bucket image in an img element.
print("<img src='" . CLOUDFRONT_URL . $imageFileName . "' style='max-width: 100%; max-height: 100%;' /><br><br>");

print("Use Linkit! to share images with friends with links.  Visit <a href='index.php'>Linkit!</a> to get started.");

?>

</body>
</html>
