<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Linkit! Image Links</title>
	<style>
	  body {color: #fff; background-color: #333; font-family: Arial, sans-serif; font-size: large; text-align: center;}
	  a   {color: #fff; background-color: transparent;}
	</style>
</head>
<body>

<?PHP
include("common.php");
require_once('vendor/autoload.php');
use Aws\S3\S3Client; 

// Get the IP address of the client.
$ipOfClient = get_client_ip_address();

// Debug IP addresses
//print("<br><br>" . $ipOfClient . "<br><br>");
//print("<br><br>'HTTP_CLIENT_IP': " . $_SERVER['HTTP_CLIENT_IP'] . "<br>");
//print("'HTTP_X_FORWARDED_FOR': " . $_SERVER['HTTP_X_FORWARDED_FOR'] . "<br>");
//print("'REMOTE_ADDR': " . $_SERVER['REMOTE_ADDR'] . "<br><br>");


// If this script wasn't visited on a HTTP POST, then redirect the user to Linkit! home.
if (!isset($_POST["submit"])) {
	header("Location: " . HOME_URL);
	die();	
}

// Make sure a file was uploaded.  If not, redirect to home page.
if (!isset($_FILES[UPLOAD_FILE_KEY])) {
	header("Location: " . HOME_URL);
	die();	
}
	
try
{
	// Get a S3Client instance for our configuration.
	$S3 = S3Client::factory(array('key'    => AWS_PUBLIC_KEY,
								'secret' => AWS_SECRET_KEY,
								'region' => BUCKET_REGION));
}
catch (Exception $e)
{
	displayErrorAndExit("Error creating S3 client:", $e->getMessage());
}

// Check the POST upload error code.  If it's not OK, print error and exit.
$uploadErrorCode = $_FILES[UPLOAD_FILE_KEY]['error'];
if ($uploadErrorCode != UPLOAD_ERR_OK) {

	$phpFileUploadErrors = array(
		0 => 'There is no error, the file uploaded with success',
		1 => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
		2 => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
		3 => 'The uploaded file was only partially uploaded',
		4 => 'No file was uploaded',
		6 => 'Missing a temporary folder',
		7 => 'Failed to write file to disk.',
		8 => 'A PHP extension stopped the file upload.',
	);

	displayErrorAndExit("There was an error in the POST upload.  Code: ", $uploadErrorCode . " - " . $phpFileUploadErrors[$uploadErrorCode]);
}

// Declare a variable that holds the temp filename.
$tmpUploadedFile = $_FILES[UPLOAD_FILE_KEY]['tmp_name'];

// Check to ensure that this was uploaded via HTTP POST, if not exit.
if (!is_uploaded_file($tmpUploadedFile)) {
	displayErrorAndExit("The file was not uploaded via HTTP POST.", "Do not reference this PHP page directly, use the HTML page to initiate an upload.");
}

// Get image information about the uploaded file.
// This returns false if the file is not an image.
$imageInfo = getimagesize($tmpUploadedFile);

// If the file wasn't an image, report error and exit.
if (!$imageInfo) {
	displayErrorAndExit("The file was not an image file.", "Linkit! only support the upload of image files for link sharing.");
}

// Get the mime type of the image.
$mime_type = $imageInfo['mime'];
switch ($mime_type) {
	case "image/jpg":
	case "image/jpeg":
		$fileExtension = ".jpg";
		break;
	case "image/gif":
		$fileExtension = ".gif";
		break;
	case "image/png":
		$fileExtension = ".png";
		break;
	default:
		displayErrorAndExit("Not a JPG, GIF, or PNG image.", "Linkit! only support the upload of JPG, GIF, or PNG images for link sharing.");
}

// Create a random base filename and use an extension that matches
// the MIME type. Example: 54d69a8b36ec2.jpg
// Note: .jpg is always used, despite the actual image type.
do {
	$uuidFilename = uniqid() . $fileExtension;
	// Check the database to ensure the name doesn't exist.
	$query = sprintf("SELECT COUNT(*) FROM audit_upload WHERE filename LIKE '%s'", $uuidFilename);
	$result = issue_db_query($query);
	$found = mysql_result($result, 0, 0);
} while ($found == 1);
	
try {
	// Upload the temporary uploaded file to the S3 bucket
	$upload = $S3->upload(BUCKET_NAME, $uuidFilename, fopen($tmpUploadedFile, 'rb'), 'public-read');
} catch(Exception $e) {
	// Failure in the S3 upload.
	displayErrorAndExit("Upload of image to the S3 bucket failed.  Error message: ", $e->getMessage());
}

// Build the auditing insertion statement.
$insertStmt = sprintf("INSERT INTO audit_upload (ip, filename, sizeInBytes) VALUES ('%s', '%s', %s)", $ipOfClient, $uuidFilename, $_FILES[UPLOAD_FILE_KEY]['size']);

// Issue the query to the database.
issue_db_query($insertStmt);
	
// Return the link to the image.
$viewURL = VIEW_URL . $uuidFilename;
$shortenedUrl = shorten_url($viewURL);

print("<br><h1>Upload successful! - Thanks for using Linkit!</h1><br><br>");
print("Use this URL any time to view your image:<br><a href='" . $viewURL . "' target='_blank'>" . $viewURL . "</a><br><br>");

if (!empty($shortenedUrl)) {
	print("Want a shorter URL instead? Use this: <br><a href='" . $shortenedUrl . "' target='_blank'>" . $shortenedUrl . "</a><br><br>");
}

print("Copy and paste this link into your favorite text based chat or email to share the image with your friends.");

?>

</body>
</html>
