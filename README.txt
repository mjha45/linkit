Linkit! is an image sharing service that allows a user to upload an image to which a link is returned for sharing textually as a URL.
This is useful for text based chat clients that don't have built-in image support, so that images can still be shared with participants.
It works great for text only emails also.
Link: http://linkit.cupcakefu.com/