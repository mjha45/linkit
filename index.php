<?PHP include("common.php"); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Linkit! - Upload and Share an Image">
    <meta name="author" content="Group A">
	<link href="favicon.ico" rel="Linkit" type="image/x-icon" />
	
    <title>Linkit! - Upload and Share an Image</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="bootstrap/css/cover.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">

          <div class="masthead clearfix">
            <div class="inner">
              <h3 class="masthead-brand">Linkit!</h3>
              <nav>
                <ul class="nav masthead-nav">
                  <li class="active"><a href="#">Home</a></li>
                  <li><a href="about.php">About</a></li>
                </ul>
              </nav>
            </div>
          </div>

          <div class="inner cover">
            <h1 class="cover-heading">Linkit! - Send a picture!</h1>
            <br />
            <p class="lead">
            
                <form class="form-inline" action="upload.php" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="userfile">Chose image to share</label>
                        <input type="file" class="btn btn-large" name="userfile" id="userfile">
						<br><br>
                        <input type="submit" class="btn btn-lg btn-default" value="Upload and Share Image" name="submit">
                    </div> 
                </form> 
            </p>
          </div>

          <div class="mastfoot">
            <div class="inner">
            <p>&copy; 2015 Linkit! v. <?PHP print("$VERSION"); ?></p>
            </div>
          </div>

        </div>

      </div>

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
	
	<script>
		// Get the form and add a submit listener that will check the file size of the
		// file in the input element.
		document.forms[0].addEventListener('submit', function( event ) {
			var file = document.getElementById('userfile').files[0];
			
			if (!file) {
				event.preventDefault();
				alert('You must select an image to upload.');
				return;
			}

			// Check for existence of file and that it's under the specified size.
			if(!(file && file.size < 20480000)) { // 20MB size limit
				// Prevent the default event handling and display an alert.
				event.preventDefault();
				alert('Image is too big so the upload will not be attempted.  Choose an image under 9MB and try again.');
				return;
			}
		}, false);
	</script>

  </body>
</html>

