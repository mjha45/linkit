<?PHP include("common.php"); ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="About Linkit!">
    <meta name="author" content="Mayank Jha">

    <title>About Linkit!</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="bootstrap/css/cover.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">

          <div class="masthead clearfix">
            <div class="inner">
              <h3 class="masthead-brand">Linkit!</h3>
              <nav>
                <ul class="nav masthead-nav">
                  <li><a href="index.php">Home</a></li>
                  <li class="active"><a href="about.php">About</a></li>
                </ul>
              </nav>
            </div>
          </div>

        <div class="inner cover">
            <h1 class="cover-heading">Linkit! - Send some pictures!</h1>
            <p class="lead">Linkit! is an application for sharing images</p>
            <div class="inner">
				<p>
					Linkit! is very useful in situations where you need to show someone an image, but the tools you are using don't have built-in image support.  For example, some web support forms allow you to describe your problem in a text field and then submit, but there's no way to attach a photo.  With Linkit!, you can upload the photo and copy/paste the link to that photo in the text field so that the image is available for the support rep.
				</p>
            </div>
          </div>
 
          <div class="mastfoot">
            <div class="inner">
            <p>&copy; 2016 Linkit! v. <?PHP print("$VERSION"); ?></p>
            </div>
          </div>

        </div>

      </div>

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>

